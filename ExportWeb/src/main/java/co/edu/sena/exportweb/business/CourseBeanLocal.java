/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/J2EE/EJB30/SessionLocal.java to edit this template
 */
package co.edu.sena.exportweb.business;

import co.edu.sena.exportweb.model.Course;
import java.io.OutputStream;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Aprendiz
 */
@Local
public interface CourseBeanLocal {
    public Course findById(Integer id) throws Exception;
    public List<Course> findAll() throws Exception;
    public void exportExcel(OutputStream outputStream) throws Exception;
}
