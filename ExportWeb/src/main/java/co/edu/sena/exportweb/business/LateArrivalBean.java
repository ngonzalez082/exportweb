/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/J2EE/EJB30/StatelessEjbClass.java to edit this template
 */
package co.edu.sena.exportweb.business;

import co.edu.sena.exportweb.model.ApprenticeCount;
import co.edu.sena.exportweb.model.LateArrival;
import co.edu.sena.exportweb.persistence.ILateArrivalDAO;
import java.util.Date;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 *
 * @author Aprendiz
 */
@Stateless
public class LateArrivalBean implements LateArrivalBeanLocal {

    @EJB
    private ILateArrivalDAO lateArrivalDAO;

    @Override
    public LateArrival findById(Integer id) throws Exception {
        if(id == 0)
        {
            throw new Exception("El id es obligatorio");
        }
        
        return lateArrivalDAO.findById(id);
    }

    @Override
    public List<LateArrival> findAll() throws Exception {
        return lateArrivalDAO.findAll();
    }

    @Override
    public List<LateArrival> findByDateRange(Date date1, Date date2) throws Exception {
        return lateArrivalDAO.findByDateRange(date1, date2);
    }

    @Override
    public List<ApprenticeCount> findGroupByApprentice() throws Exception {
        return lateArrivalDAO.findGroupByApprentice();
    }
    
    
}
