/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/J2EE/EJB30/StatelessEjbClass.java to edit this template
 */
package co.edu.sena.exportweb.business;

import co.edu.sena.exportweb.model.Apprentice;
import co.edu.sena.exportweb.persistence.IApprenticeDAO;
import co.edu.sena.exportweb.utils.Constants;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.element.Table;
import com.itextpdf.layout.properties.TextAlignment;
import java.io.OutputStream;
import java.util.List;
import java.util.Properties;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

/**
 *
 * @author Aprendiz
 */
@Stateless
public class ApprenticeBean implements ApprenticeBeanLocal {
       
    @EJB
    private IApprenticeDAO apprenticeDAO;

    @Override
    public Apprentice findById(Long document) throws Exception {
        if(document == 0)
        {
            throw new Exception("El documento es obligatorio");
        }
        
        return apprenticeDAO.findById(document);
    }

    @Override
    public List<Apprentice> findAll() throws Exception {
        return apprenticeDAO.findAll();
    }

    @Override
    public void exportPDFApprentices(OutputStream outputStream) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }   

    @Override
    public void sendEmail(String to, String subject) throws Exception {
        Properties props = new Properties();
        props.put("mail.tranport.protocol", Constants.PROTOCOL);
        props.put("mail.smtp.host", Constants.HOST);
        props.put("mail.smtp.socketFactory.port", Constants.SOCKET_FACTORY_PORT);
        props.put("mail.smtp.socketFactory.class", Constants.SOCKET_FACTORY);
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.port", Constants.PORT);
        props.put("mail.smtp.user", Constants.USER);
        props.put("mail.smtp.password", Constants.PASSWORD);
        props.put("mail.smtp.starttls.enable", "true");
        
        Session session = Session.getDefaultInstance(props);
        MimeMessage message = new MimeMessage(session);
        String bodyMessage = setMessage();
        InternetAddress from = new InternetAddress(props.getProperty("mail.smtp.user"));
        message.setFrom(from); //Remitente
        message.setSubject(subject); //Asunto
        message.addRecipients(Message.RecipientType.TO, InternetAddress.parse(to)); //Destinatario
        message.setContent(bodyMessage, "text/html; charset=utf-8"); //Cuerpo mensaje
        
        Transport transport = session.getTransport(Constants.PROTOCOL);
        transport.connect(props.getProperty("mail.smtp.host"), props.getProperty("mail.smtp.user"), props.getProperty("mail.smtp.password"));
        transport.sendMessage(message, message.getAllRecipients());
        transport.close();
    }
    
    public String setMessage() throws Exception{
        String message = "";
        try {
            List<Apprentice> apprentices = findAll();
            message = "<p>Cordial Saludo, se envia el reporte de aprendices registrados en ARLET</p>" +
                    "<table border='1'>" + 
                    "<tr> <th>Documento</th> <th>Nombre</th> <th>Programa</th> </tr>";
            for (Apprentice apprentice : apprentices){
                message += "<tr>"+
                                "<td>" + apprentice.getDocument()+ "</td>" +
                                "<td>" + apprentice.getFullName()+ "</td>" +
                                "<td>" + apprentice.getIdCourse().getCareer()+ "</td>" +
                            "</tr>";
            }
            message += "</table>";
            
        } catch (Exception e) {
            throw e;
        }
        return message;
    }
    
}
